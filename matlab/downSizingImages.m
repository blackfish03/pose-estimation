clear all; close all; clc;
filenames = dir('.\*.png');
leng = length(filenames);

for i=160:160+(leng-1)
    name = '0.png';
    name = 	strrep(name, num2str(0), num2str(i));
    img = imread(name);
    img = img(1:2:end, 1:2:end, :);
    imwrite(img,name);
end