%% This script is used to prepare data for DL
for i=1:2:length(A)
    plot(A(i),A(i+1),'x');
    text(A(i),A(i+1),num2str(i));
    hold on;
end
hold off
%%
clc; clear all;
raw = csvread('test_body.csv');
scalar = 100;
file_names=[];
startIndex = 1:length(raw);
headIndex = 10:11; neckIndex = 7:8; spineMidIndex = 4:5; 
LShoulderIndex = 13:14; LElbowIndex = 16:17; LHandIndex = 22:23;
RShoulderIndex = 25:26; RElbowIndex =28:29; RHandIndex = 34:35;
LHipIndex = 37:38; LKneeIndex = 40:41; LAnkleIndex = 43:44;
RHipIndex = 49:50; RKneeIndex = 52:53; RAnkleIndex = 55:56;

head = raw(startIndex, headIndex) * scalar;
neck = raw(startIndex, neckIndex)* scalar;
spine = raw(startIndex, spineMidIndex)* scalar;
lShoulder = raw(startIndex, LShoulderIndex)* scalar;
lElbow = raw(startIndex, LElbowIndex)* scalar;
lHand = raw(startIndex, LHandIndex)* scalar;
rShoulder = raw(startIndex, RShoulderIndex)* scalar;
rElbow = raw(startIndex, RElbowIndex)* scalar;
rHand = raw(startIndex, RHandIndex)* scalar;
lHip = raw(startIndex, LHipIndex)* scalar;
lKnee = raw(startIndex, LKneeIndex)* scalar;
lAnkle = raw(startIndex, LAnkleIndex)* scalar;
rHip = raw(startIndex, RHipIndex)* scalar;
rKnee = raw(startIndex, RKneeIndex)* scalar;
rAnkle = raw(startIndex, RAnkleIndex)* scalar;

appendOne = ones(length(startIndex),1);

% for j=1:length(startIndex)
%     temp = [num2str(startIndex(j)), '.png,'];
%     file_names = [file_names; temp];
% end

Compiled = [head, neck, spine, lShoulder, lElbow, lHand, rShoulder, rElbow, rHand, lHip, lKnee, lAnkle, rHip, rKnee, rAnkle, appendOne];

filename = 'DL-ProcessedData.txt';
delete 'DL-ProcessedData.txt';
% for i=1:length(startIndex)
    dlmwrite(filename, Compiled, '-append');
% end

% fid = fopen(filename, 'r');
% data = cell(1,length(Compiled(:,1)));
% 
% for k=1:length(Compiled(:,1))
%     data{k} = fgetl(fid);
%     data{k} = [file_names(k),data{k}]
% end
% fclose(fid);


